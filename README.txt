  -- SUMMARY --

  This is a simple module that restricts the feeds module to a single import


  -- REQUIREMENTS --

  * You have feeds installed.

  -- INSTALLATION --

  * Install as usual, see http://drupal.org/node/895232 for further information.



  -- CONFIGURATION --

 change the number if you want in the module file

  -- CONTACT --

  Current maintainers:
  * Arne Olafson - http://www.drupal.org/user/661268
